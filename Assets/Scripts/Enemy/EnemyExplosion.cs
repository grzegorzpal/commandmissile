using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyExplosion : MonoBehaviour
{
    [SerializeField] private Animator m_animator;

    public void Explode()
    {
        m_animator.SetTrigger("boom");
    }
}
