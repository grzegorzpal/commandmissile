using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class EnemiesController : MonoBehaviour
{

    [SerializeField, Header("Enemies moving board")] private float m_rightBoard, m_leftBoard;
    [SerializeField] private EnemyMissile m_enemyMissile;

    private bool m_randomDirection;
    private ObjectPool<EnemyMissile> m_objectPool;
    void Start()
    {
        m_objectPool = new ObjectPool<EnemyMissile>(() =>
        {
            return Instantiate(m_enemyMissile, transform.position, Quaternion.identity, transform);
        }, missile =>
        {
            missile.gameObject.SetActive(true);
        }, missile =>
        {
            missile.gameObject.SetActive(false);
        }, missile =>
        {
            Destroy(missile.gameObject);
        }, true, 10, 20);
        int a = Random.Range(0, 1);

        m_randomDirection = a == 0 ? true : false;

        StartCoroutine(CrateMissile());
    }

    IEnumerator CrateMissile()
    {
        yield return new WaitForSeconds(3);
        EnemyMissile enemyMissile = m_objectPool.Get();
        enemyMissile.SetObjectPoolVariable(m_objectPool);
    }

    private void Update()
    {
        
    }
}
