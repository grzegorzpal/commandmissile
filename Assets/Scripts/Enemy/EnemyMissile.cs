using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class EnemyMissile : MonoBehaviour, IDamage
{
    private ObjectPool<EnemyMissile> m_objectPool;

    [SerializeField] private int m_damage;
    public int Damage 
    { 
        get
        {
            return m_damage;
        }
    }

    void Update()
    {
        transform.Translate(Vector3.down * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        m_objectPool.Release(this);
    }

    public void SetObjectPoolVariable(ObjectPool<EnemyMissile> objectPool)
    {
        m_objectPool = objectPool;
    }
}
