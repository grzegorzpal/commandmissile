using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Linq;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private InputActionReference m_leftMouseButton;
    [SerializeField] private Weapon[] m_currentWeapon;

    private Weapon m_weapon;
    void Update()
    {
        if (m_leftMouseButton.action.triggered)
        {
            //Should count distance beetwen weapon and point click
            // m_currentWeapon.Attack(Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue()));
            m_weapon = NearestWeaponToClickPoint(Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue()));
            m_weapon.Attack(Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue()));
            m_weapon = null;
        }
    }

    private Weapon NearestWeaponToClickPoint(Vector2 clickPoint)
    {
        Dictionary<Weapon, float> weaponsDistance = new Dictionary<Weapon, float>();
        for (int i = 0; i < m_currentWeapon.Length; i++)
        {
            if (m_currentWeapon[i].CanAttack)
            {
                float distance = Vector2.Distance(clickPoint, m_currentWeapon[i].transform.position);
                weaponsDistance.Add(m_currentWeapon[i], distance);
            }
        }

        return weaponsDistance.OrderBy(x => x.Value).ToDictionary(x => x.Key, y => y.Value).First().Key;
    }
}
