using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationObjectExplosion : MonoBehaviour
{
    [SerializeField] private Missile m_misille;
    [SerializeField] private Animator m_explosionAnimator;

    public void RealaseObject()
    {
        m_explosionAnimator.Rebind();
        m_explosionAnimator.ResetTrigger("boom");
        m_misille.DisableObject();
        gameObject.SetActive(false);
    }
}
