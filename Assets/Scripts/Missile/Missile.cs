using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public abstract class Missile : MonoBehaviour
{
    [SerializeField] private float m_speed;
    [SerializeField] protected SpriteRenderer m_spriteRenderer;

    protected bool m_canMove = true;
    private ObjectPool<Missile> m_objectPool;

    private Vector3 m_startPosition;
    protected Vector3 m_mousePos;

    public abstract void Explode();
    protected void Awake()
    {
        m_startPosition = transform.position;
    }
    protected virtual void OnEnable()
    {
        m_canMove = true;
    }
    public virtual void SetMousePosition(Vector3 mousePos)
    {
        m_mousePos = mousePos;
    }

    protected void Update()
    {
        if (m_canMove)
        {
            transform.position = Vector3.MoveTowards(transform.position, m_mousePos, m_speed * Time.deltaTime);
        }

        if (transform.position == m_mousePos)
        {
            Explode();
        }
    }

    public void DisableObject()
    {
        m_objectPool.Release(this);
        transform.position = m_startPosition;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Explode();
    }

    public void SetObjectPoolVariable(ObjectPool<Missile> objectPool)
    {
        m_objectPool = objectPool;
    }

    public virtual void OnGetFromPool()
    {
        m_spriteRenderer.enabled = true;
    }
    public virtual void OnRealaseToPool()
    {

    }
}