using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class BigMissile : Missile
{
    [SerializeField] private GameObject m_explosionObject;
    [SerializeField] private Animator m_explosionAnimator;
    [SerializeField] private float m_explosionRange;
    public override void Explode()
    {
        m_explosionObject.SetActive(true);
        m_spriteRenderer.enabled = false;
        m_explosionAnimator.SetTrigger("boom");
        m_canMove = false;
        GetObjectsInCircle();
    }

    private void GetObjectsInCircle()
    {
        RaycastHit2D[] raycastHit2D = Physics2D.CircleCastAll(this.transform.position, m_explosionRange, Vector2.zero);
        
        for (int i = 0; i < raycastHit2D.Length; i++)
        {
            if (raycastHit2D[i].collider.GetComponent<EnemyExplosion>())
            {
                raycastHit2D[i].collider.GetComponent<EnemyExplosion>().Explode();
            }
        }
    }
}
