using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class CreatePoolExplosionObjects : MonoBehaviour
{
    [SerializeField] private RealaseObjectExplosion m_realaseObjectExplosion;
    [SerializeField] private Transform m_playerMissileParent;

    private ObjectPool<RealaseObjectExplosion> m_objectPool;
    void Start()
    {
        m_objectPool = new ObjectPool<RealaseObjectExplosion>(() =>
        {
            return Instantiate(m_realaseObjectExplosion, transform.position, Quaternion.Euler(0, 0, 0), m_playerMissileParent);
        }, shape =>
        {
            shape.gameObject.SetActive(true);
        }, shape =>
        {
            shape.gameObject.SetActive(false);
        }, shape =>
        {
            Destroy(shape.gameObject);
        }, true, 10, 20);
    }

   
}
