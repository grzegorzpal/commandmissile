using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour, IHealth
{
    [SerializeField] private int m_health;

    public int Health 
    {
        get
        {
            return m_health;
        }
    }

    public void Die()
    {
        Destroy(gameObject);
    }

    public void SubstractHealth(int healthPoint)
    {
        m_health -= healthPoint;

        Debug.Log(m_health);
        if (Health < 0)
            Die();
    }
   

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(1234);
        if (collision.GetComponent<EnemyMissile>() == null) return;

        EnemyMissile enemyMissile = collision.GetComponent<EnemyMissile>();

        SubstractHealth(enemyMissile.Damage);
    }
}
