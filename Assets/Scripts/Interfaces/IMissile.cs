using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public interface IMissile
{
    ObjectPool<Missile> ObjectPool { get; }

    void SetVariable(ObjectPool<Missile> objectPool);
}
