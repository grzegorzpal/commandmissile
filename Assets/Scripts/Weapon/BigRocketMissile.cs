using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class BigRocketMissile : Weapon
{
    [SerializeField] private int m_missileAmountOfLanucher;
    [SerializeField] private BigMissile m_bigMissile;
    [SerializeField] private Transform m_playerMissileParent;

    private ObjectPool<Missile> m_objectPool;
    private int m_shootedMissileAmount;

    public override bool CanAttack => m_shootedMissileAmount < m_missileAmountOfLanucher;

    private void Start()
    {
        m_objectPool = new ObjectPool<Missile>(() =>
        {
            return Instantiate(m_bigMissile, transform.position, Quaternion.identity, m_playerMissileParent);
        }, missile =>
        {
            missile.gameObject.SetActive(true);
            missile.OnGetFromPool();
        }, missile =>
        {
            missile.gameObject.SetActive(false);
        }, missile =>
        {
            Destroy(missile.gameObject);
        }, true, 10, 20);

    }

    public override void Attack(Vector2 mousePos)
    {
        if (!CanAttack) return;

        Missile bigMissle = m_objectPool.Get();
        bigMissle.SetObjectPoolVariable(m_objectPool);
        bigMissle.SetMousePosition(mousePos);
        m_shootedMissileAmount++;

    }
}
