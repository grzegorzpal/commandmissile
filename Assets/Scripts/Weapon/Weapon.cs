using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    public abstract void Attack(Vector2 mousePos);

    public virtual bool CanAttack { get; protected set; }
}
